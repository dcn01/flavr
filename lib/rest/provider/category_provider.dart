import 'dart:async';

import 'package:flavr/model/category.dart';
import 'package:flavr/rest/api_client.dart';

abstract class CategoryProvider {
  Future<List<Category>> loadCategory({int page: 1});
}

class CProvider extends CategoryProvider {
  ApiClient _apiClient = ApiClient();

  @override
  Future<List<Category>> loadCategory({int page: 1}) {
    return _apiClient.fetchCategoryList(page: page);
  }
}
