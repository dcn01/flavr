import 'dart:async';

import 'package:flavr/model/recipes.dart';
import 'package:flavr/rest/api_client.dart';

abstract class RecipesProvider {
  Future<List<Recipes>> loadRecipes(int categoryId, {int page: 1});
}

class RProvider extends RecipesProvider {
  ApiClient _apiClient = ApiClient();

  @override
  Future<List<Recipes>> loadRecipes(int categoryId, {int page: 1}) {
    return _apiClient.fetchRecipesList(categoryId, page: page);
  }
}
