import 'dart:convert';
import 'dart:io';

import 'package:flavr/model/category.dart';
import 'package:flavr/model/kitchen_accessories.dart';
import 'package:flavr/model/recipes.dart';

class ApiClient {
  static final _client = ApiClient._internal();
  final _http = HttpClient();

  ApiClient._internal();

  final String baseUrl = "devshub.co";

  factory ApiClient() => _client;

  Future<dynamic> _getJson(Uri uri) async {
    var response = await (await _http.getUrl(uri)).close();
    var transformResponse = await response.transform(utf8.decoder).join();
    return json.decode(transformResponse);
  }

  // Get Kitchen Accessories List
  Future<List<KitchenAccessories>> fetchKitchenAccessories({int page: 1}) {
    var url = Uri.https(baseUrl, 'api/v1/getAllKitcheAccessoriesList',
        {'page': page.toString()});
    return _getJson(url).then((json) => json['data']).then((data) => data
        .map<KitchenAccessories>((item) => KitchenAccessories(item))
        .toList());
  }

  // Get Recipes List
  Future<List<Recipes>> fetchRecipesList(int categoryId, {int page: 1}) {
    var url = Uri.https(baseUrl, 'api/v1/getAllRecipesList',
        {'categoryId': categoryId.toString(), 'page': page.toString()});

    return _getJson(url)
        .then((json) => json['data'])
        .then((data) => data.map<Recipes>((item) => Recipes(item)).toList());
  }

  // Get Category List
  Future<List<Category>> fetchCategoryList({int page: 1}) {
    var url = Uri.https(
        baseUrl, 'api/v1/getAllCategoryList', {'page': page.toString()});
    return _getJson(url)
        .then((json) => json['data'])
        .then((data) => data.map<Category>((item) => Category(item)).toList());
  }
}
