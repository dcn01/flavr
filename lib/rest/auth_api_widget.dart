import 'dart:convert';

import 'package:flavr/model/user.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as HTTP;
import 'package:http/http.dart';

class AuthApiWidget extends InheritedWidget {
  static final String _BASE_URL = "https://devshub.co/api/v1";
  static const _TIMEOUT = Duration(seconds: 60);

  AuthApiWidget({Key key, @required Widget child})
      : assert(child != null),
        super(key: key, child: child);

  static AuthApiWidget of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(AuthApiWidget) as AuthApiWidget;
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return false;
  }

  Future<dynamic> loginUser({Map body}) async {
    var url = '$_BASE_URL/login';
    final response =
        await HTTP.post(
            url,
            headers: {"Content-Type": "application/json"},
            body: body
        ).timeout(_TIMEOUT);
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      badStatusCode(response);
    }
  }

  Future<dynamic> registerUser(User user) async {
    var url = '$_BASE_URL/register';
    final response =
        await HTTP.post(url, body: json.encode(user)).timeout(_TIMEOUT);
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      badStatusCode(response);
    }
  }

  badStatusCode(Response response) {
    debugPrint("Bad status code ${response.statusCode} returned from server.");
    debugPrint("Response body ${response.body} returned from server.");
    throw Exception(
        "Bad status code ${response.statusCode} returned from server.");
  }
}
