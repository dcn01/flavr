import 'package:flavr/model/recipes.dart';
import 'package:flavr/model/category.dart';
import 'package:flavr/model/kitchen_accessories.dart';
// Singleton Class
class InMemoryStore {
  static final InMemoryStore _inMemoryStore = InMemoryStore._construct();

  factory InMemoryStore() {
    return _inMemoryStore;
  }

  InMemoryStore._construct();

  List<Recipes> recipesList = List();
  List<KitchenAccessories> kitchenAccessoriesList = List();
  List<Category> categoryList = List();

  void clear() {
    recipesList.clear();
    kitchenAccessoriesList.clear();
    categoryList.clear();
  }
}