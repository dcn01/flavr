import 'package:flavr/utils/colors.dart';
import 'package:flavr/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:flavr/db/db_helper.dart';
import 'package:flavr/model/favorite.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';

class FavoriteTab extends StatefulWidget {
  @override
  _FavoriteTabState createState() => _FavoriteTabState();
}

class _FavoriteTabState extends State<FavoriteTab> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  DbHelper dbHelper = DbHelper();
  List<Favorite> favoriteList;

  _showSnackBar(String content, Color color, {bool error = false}) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      backgroundColor: color,
      content: Text(
        '${error ? "An unexpecte error occurred : " : " "} $content',
        style: TextStyle(color: colorWhite),
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    if (favoriteList == null) {
      favoriteList = List<Favorite>();
      getFavoriteRecipes();
    }
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: colorMainBackground,
        body: checkState()
    );
  }

  Widget checkState() {
     Widget favWidget;
     if (favoriteList.length <= 0) {
       favWidget = emptyViewWidget();
     } else {
       favWidget = favListWidget();
     }
     return favWidget;
  }

  Widget favListWidget() {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: favoriteList.length,
        itemBuilder: (BuildContext context, int index) {
          return _favoriteItems(favoriteList[index], context);
    });
  }

  Widget emptyViewWidget() {
    return Center(
      child: Container(
        padding: EdgeInsets.only(top: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.favorite,
              color: Colors.redAccent,
              size: 40.0,
            ),
            Text(
              'No Favorite Recipes yet...',
              style: TextStyle(fontSize: 18.0, fontFamily: fontBold),
            )
          ],
        ),
      ),
    );
  }

  Widget _favoriteItems(Favorite favorite, context) {

    return Container(
      child: Card(
        child: GestureDetector(
          onTap: () async => await FlutterWebBrowser.openWebPage(
            url: "https://devshub.co/",
            androidToolbarColor: Theme.of(context).primaryColor,
          ),
          child: Stack(
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Hero(
                    child: FadeInImage.assetNetwork(
                      placeholder: "assets/images/recipes_placeholder.png",
                      image: favorite.photo,
                      fit: BoxFit.fill,
                      fadeInDuration: Duration(milliseconds: 50),
                    ),
                    tag: "Favorite-Tag-${favorite.id}",
                  ),
                  Padding(
                    padding: EdgeInsets.all(12.0),
                    child: Text(
                      favorite.title,
                      style: TextStyle(
                          fontFamily: fontBold,
                          fontSize: 20.0,
                          color: Colors.grey,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  onTap: () {
                    removeFavorite(favorite.id);
                    getFavoriteRecipes();
                  },
                  child: Container(
                    padding: EdgeInsets.all(6.0),
                    child: Icon(
                      Icons.favorite,
                      color: Colors.redAccent,
                      size: 28,
                    ),
                  ),
                ),
              )
            ],
          )
        ),
      ),
    );
  }

  void getFavoriteRecipes() {
    final dbFuture = dbHelper.initializeDb();
    dbFuture.then((result){
      final recipeFuture = dbHelper.getRecipes();
      recipeFuture.then((result){
        List<Favorite> favList = List<Favorite>();
        for (int i = 0; i < result.length; i++) {
          favList.add(Favorite.fromObject(result[i]));
        }
        setState(() {
          favoriteList = favList;
        });
      });
    });
  }

  void removeFavorite(int favId) {
    final removeResult = dbHelper.deleteRecipe(favId);
    removeResult.then((result){
      if(result == 0) {
        _showSnackBar('Already removed recipe', Colors.orange);
      } else {
        _showSnackBar('Recipe removed successfully.', Colors.green);
      }
    });
  }
}
