import 'package:flavr/model/kitchen_accessories.dart';
import 'package:flavr/model/recipes.dart';
import 'package:flavr/rest/provider/kitchen_accessories_provider.dart';
import 'package:flavr/rest/provider/recipes_provider.dart';
import 'package:flavr/utils/colors.dart';
import 'package:flavr/utils/constants.dart';
import 'package:flavr/utils/in_memory_store.dart';
import 'package:flavr/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:flavr/db/db_helper.dart';
import 'package:flavr/model/favorite.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flavr/utils/navigator.dart';

DbHelper dbHelper = DbHelper();

class RecipesTab extends StatefulWidget {
  RecipesTab({Key key}) : super(key: key);

  @override
  _RecipesTabState createState() => _RecipesTabState();
}

class _RecipesTabState extends State<RecipesTab> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  // Recipes
  final RProvider rProvider = RProvider();
  List<Recipes> _recipeList = InMemoryStore().recipesList;
  int _rPageNumber = 1;
  LoadingState _rLoadingState = LoadingState.LOADING;
  bool _rIsLoading = false;

  // Kitchen Accessories
  final KaProvider kaProvider = KaProvider();
  List<KitchenAccessories> _kaList = InMemoryStore().kitchenAccessoriesList;
  int _kaPageNumber = 1;
  LoadingState _kaLoadingState = LoadingState.LOADING;
  bool _kaIsLoading = false;

  String _name;
  String _email;

  getSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _name = prefs.getString('name');
      _email = prefs.getString('email');
    });
  }

  _showSnackBar(String content, Color color, {bool error = false}) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      backgroundColor: color,
      content: Text(
        '${error ? "An unexpecte error occurred : " : " "} $content',
        style: TextStyle(color: colorWhite),
      ),
    ));
  }

  @override
  void initState() {
    super.initState();

    if (_kaList.isEmpty) {
      _loadKaPage();
    } else {
      _kaLoadingState = LoadingState.DONE;
    }

    if (_recipeList.isEmpty) {
      _loadRecipes();
    } else {
      _rLoadingState = LoadingState.DONE;
    }

    getSharedPreferences();
  }

  _loadKaPage() async {
    _kaIsLoading = true;
    try {
      var getKaList =
          await kaProvider.loadKitchenAccessories(page: _kaPageNumber);
      setState(() {
        _kaLoadingState = LoadingState.DONE;
        _kaList.addAll(getKaList);
        _kaIsLoading = false;
        _kaPageNumber++;
      });
    } catch (e) {
      _kaIsLoading = false;
      if (_kaLoadingState == LoadingState.LOADING) {
        setState(() {
          _kaLoadingState = LoadingState.ERROR;
        });
      }
    }
  }

  _loadRecipes() async {
    _rIsLoading = true;
    try {
      var getRecipesList = await rProvider.loadRecipes(0, page: _rPageNumber);
      setState(() {
        _rLoadingState = LoadingState.DONE;
        _recipeList.addAll(getRecipesList);
        _rIsLoading = false;
        _rPageNumber++;
      });
    } catch (e) {
      _rIsLoading = false;
      if (_rLoadingState == LoadingState.LOADING) {
        setState(() {
          _rLoadingState = LoadingState.ERROR;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: colorMainBackground,
      body: ListView(
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(left: 12.0, top: 12.0),
                  child: Text(
                    'Kitchen Accessories',
                    style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.w600,
                        fontFamily: fontRegular),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 12.0, top: 6.0),
                  height: 200.0,
                  child: Center(
                    child: _getKaContent(),
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(left: 12.0, top: 12.0, bottom: 12.0),
                  child: Text(
                    'Latest Recipes',
                    style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.w600,
                        fontFamily: fontRegular),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 12.0, right: 12.0),
                  child: Center(
                    child: _getRecipesContent(),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _kitchenAccessoriesItems(KitchenAccessories ka, context) {
    return Container(
        child: Card(
      child: GestureDetector(
        onTap: () async => await FlutterWebBrowser.openWebPage(
            url: "https://devshub.co/",
            androidToolbarColor: Theme.of(context).primaryColor),
        child: Hero(
          child: FadeInImage.assetNetwork(
            placeholder: "assets/images/kitchen_placeholder.png",
            image: ka.photo,
            fit: BoxFit.fill,
            width: 140.0,
            height: 180.0,
            fadeInDuration: Duration(milliseconds: 50),
          ),
          tag: "Movie-Tag-${ka.id}",
        ),
      ),
    ));
  }

  Widget _latestRecipesItems(Recipes recipe, context) {
    return Container(
      child: Card(
        child: GestureDetector(
            onTap: () async => await FlutterWebBrowser.openWebPage(
                  url: "https://devshub.co/",
                  androidToolbarColor: Theme.of(context).primaryColor,
                ),
            child: Stack(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Hero(
                      child: FadeInImage.assetNetwork(
                        placeholder: "assets/images/recipes_placeholder.png",
                        image: recipe.photo,
                        fit: BoxFit.fill,
                        width: double.infinity,
                        fadeInDuration: Duration(milliseconds: 50),
                      ),
                      tag: "Recipe-Tag-${recipe.id}",
                    ),
                    Padding(
                      padding: EdgeInsets.all(12.0),
                      child: Text(
                        recipe.title,
                        style: TextStyle(
                            fontFamily: fontBold,
                            fontSize: 20.0,
                            color: Colors.grey,
                            fontWeight: FontWeight.w700),
                      ),
                    )
                  ],
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: GestureDetector(
                    onTap: () {
                      Favorite fav = Favorite(recipe.id, recipe.categoryId,
                          recipe.categoryName, recipe.title, recipe.photo);
                      saveRecipe(fav);
                    },
                    child: Container(
                      padding: EdgeInsets.all(6.0),
                      child: Icon(
                        Icons.favorite_border,
                        color: Colors.redAccent,
                        size: 28,
                      ),
                    ),
                  ),
                )
              ],
            )),
      ),
    );
  }

  Widget _getRecipesContent() {
    switch (_rLoadingState) {
      case LoadingState.DONE:
        return ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: _recipeList.length,
            itemBuilder: (BuildContext context, int index) {
              if (!_rIsLoading && index > (_recipeList.length * 0.7)) {
                _loadRecipes();
              }
              return _latestRecipesItems(_recipeList[index], context);
            });
      case LoadingState.ERROR:
        return GestureDetector(
          onTap: () {
            _showSnackBar('Reconnecting with server',Colors.green);
            _rPageNumber = 1;
            _loadRecipes();
          },
          child: Container(
            padding: EdgeInsets.only(top: 20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.cloud_off,
                  color: Colors.redAccent,
                  size: 40.0,
                ),
                Text(
                  'No Internet Connection.',
                  style: TextStyle(fontSize: 18.0, fontFamily: fontBold),
                ),
                Text(
                  'Click to Retry',
                  style: TextStyle(color: Colors.deepOrange),
                ),
              ],
            ),
          ),
        );
      case LoadingState.LOADING:
        return CircularProgressIndicator();
      default:
        return Container();
    }
  }

  Widget _getKaContent() {
    switch (_kaLoadingState) {
      case LoadingState.DONE:
        return ListView.builder(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            itemCount: _kaList.length,
            itemBuilder: (BuildContext context, int index) {
              if (!_kaIsLoading && index > (_kaList.length * 0.7)) {
                _loadKaPage();
              }
              return _kitchenAccessoriesItems(_kaList[index], context);
            });
      case LoadingState.ERROR:
        return GestureDetector(
          onTap: () {
            _showSnackBar('Reconnecting with server',Colors.green);
            _kaPageNumber = 1;
            _loadKaPage();
          },
          child: Container(
            padding: EdgeInsets.only(top: 20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.cloud_off,
                  color: Colors.redAccent,
                  size: 40.0,
                ),
                Text(
                  'No Internet Connection.',
                  style: TextStyle(fontSize: 18.0, fontFamily: fontBold),
                ),
                Text(
                  'Click to Retry',
                  style: TextStyle(color: Colors.deepOrange),
                ),
              ],
            ),
          ),
        );
      case LoadingState.LOADING:
        return CircularProgressIndicator();
      default:
        return Container();
    }
  }
  
  void saveRecipe(Favorite fav) {
    if (_name == null || _email == null ||
        _name.isEmpty || _email.isEmpty) {
      goToLoginScreen(context);
    } else if (_name.isNotEmpty && _email.isNotEmpty) {
      final saveResult = dbHelper.insertRecipe(fav);
      saveResult.then((result){
        if(result == 0) {
          _showSnackBar('Already saved recipe', Colors.orange);
        } else {
          _showSnackBar('Recipe saved successfully.', Colors.green);
        }
      });
    }

  }
}
