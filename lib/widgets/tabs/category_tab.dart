import 'package:flavr/model/category.dart';
import 'package:flavr/rest/provider/category_provider.dart';
import 'package:flavr/utils/colors.dart';
import 'package:flavr/utils/constants.dart';
import 'package:flavr/utils/navigator.dart';
import 'package:flavr/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:flavr/utils/in_memory_store.dart';

class CategoryTab extends StatefulWidget {
  @override
  _CategoryTabState createState() => _CategoryTabState();
}

class _CategoryTabState extends State<CategoryTab> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final CProvider cProvider = CProvider();
  List<Category> _categoryList = InMemoryStore().categoryList;
  int _pageNumber = 1;
  LoadingState _loadingState = LoadingState.LOADING;
  bool _isLoading = false;

  _showSnackBar(String content, Color color, {bool error = false}) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      backgroundColor: color,
      content: Text(
        '${error ? "An unexpecte error occurred : " : " "} $content',
        style: TextStyle(color: colorWhite),
      ),
    ));
  }

  @override
  void initState() {
    super.initState();
    if(_categoryList.isEmpty) {
       _loadCategory();
    } else {
      _loadingState = LoadingState.DONE;
    }
  }

  _loadCategory() async {
    _isLoading = true;
    try {
      var getCategoryList = await cProvider.loadCategory(page: _pageNumber);
      setState(() {
        _loadingState = LoadingState.DONE;
        _categoryList.addAll(getCategoryList);
        _isLoading = false;
        _pageNumber++;
      });
    } catch (e) {
      _isLoading = false;
      if (_loadingState == LoadingState.LOADING) {
        setState(() {
          _loadingState = LoadingState.ERROR;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: colorMainBackground,
      body: Center(
          child: _getCategoryContent(),
      ));
  }

  Widget _categoryItems(Category category, context) {
    return Container(
      child: Card(
        child: GestureDetector(
            onTap: () => goToCategoryDetailScreen(context, category),
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Hero(
                  child: FadeInImage.assetNetwork(
                    placeholder: "assets/images/category_placeholder.png",
                    image: category.photo,
                    fit: BoxFit.fill,
                    width: double.infinity,
                    fadeInDuration: Duration(milliseconds: 50),
                  ),
                  tag: "Category-Tag-${category.id}",
                ),
                Positioned(
                    child: Container(
                  color: Colors.black.withOpacity(0.5),
                  padding: EdgeInsets.all(12.0),
                  child: Text(
                    category.name,
                    style: TextStyle(
                        fontFamily: fontBold,
                        fontSize: 20.0,
                        color: Colors.white,
                        fontWeight: FontWeight.w700),
                  ),
                ))
              ],
            )),
      ),
    );
  }

  Widget _getCategoryContent() {
    switch (_loadingState) {
      case LoadingState.DONE:
        return ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: _categoryList.length,
            itemBuilder: (BuildContext context, int index) {
              if (!_isLoading && index > (_categoryList.length * 0.7)) {
                _loadCategory();
              }
              return _categoryItems(_categoryList[index], context);
            });
      case LoadingState.ERROR:
        return GestureDetector(
          onTap: () {
            _showSnackBar('Reconnecting with server',Colors.green);
            _pageNumber = 1;
            _loadCategory();
          },
          child: Container(
            padding: EdgeInsets.only(top: 20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.cloud_off,
                  color: Colors.redAccent,
                  size: 40.0,
                ),
                Text('No Internet Connection.',
                style: TextStyle(
                  fontSize: 18.0,
                  fontFamily: fontBold
                ),
                ),
                Text('Click to Retry',
                  style: TextStyle(
                    color: Colors.deepOrange
                  ),
                ),
              ],
            ),
          ),
        );
      case LoadingState.LOADING:
        return CircularProgressIndicator();
      default:
        return Container();
    }
  }
}
