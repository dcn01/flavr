import 'package:flavr/utils/colors.dart';
import 'package:flavr/utils/navigator.dart';
import 'package:flavr/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileTab extends StatefulWidget {
  @override
  _ProfileTabState createState() => _ProfileTabState();
}

class _ProfileTabState extends State<ProfileTab> {

  String _name;
  String _email;

  _setValue(String key, String value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString(key, value);
  }

  getSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _name = prefs.getString('name');
      _email = prefs.getString('email');
    });
  }

  Text createNameWidget() {
    return Text(_name == null || _name.isEmpty ? 'cooker' : _name,
        style: TextStyle(
            color: colorMain,
            fontFamily: fontRegular,
            fontSize: 20.0));
  }

  Text createEmailWidget() {
    return Text(_email == null || _email.isEmpty ? 'cooker@gmail.com' : _email,
        style: TextStyle(
            color: Colors.grey,
            fontFamily: fontRegular,
            fontSize: 14.0)
    );
  }

  @override
  void initState() {
    super.initState();
    getSharedPreferences();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorMainBackground,
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          SizedBox(
            height: 30,
          ),
          Container(
            alignment: Alignment.center,
            child: Container(
              width: 110.0,
              height: 110.0,
              child: Image.asset('assets/images/profile.jpg'),
            ),
          ),
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 8,
                ),
                createNameWidget(),
                createEmailWidget(),
              ],
            ),
          ),
          SizedBox(
            height: 8,
          ),
          ListTile(
            onTap: () => goToAboutAppScreen(context),
            leading: Icon(Icons.info),
            title: Text("About Application"),
          ),
          Divider(
            color: Colors.grey,
            height: 1.0,
          ),
          ListTile(
            onTap: () async =>
            await FlutterWebBrowser.openWebPage(
              url: "https://devshub.co/",
              androidToolbarColor: Theme
                  .of(context)
                  .primaryColor,
            ),
            leading: Icon(Icons.assistant_photo),
            title: Text("Privacy Policy"),
          ),
          Divider(
            color: Colors.grey,
            height: 1.0,
          ),
          ListTile(
            onTap: () async =>
            await FlutterWebBrowser.openWebPage(
                url: "https://devshub.co/",
                androidToolbarColor: Theme
                    .of(context)
                    .primaryColor),
            leading: Icon(Icons.beenhere),
            title: Text("Terms and Condition"),
          ),
          Divider(
            color: Colors.grey,
            height: 1.0,
          ),
          ListTile(
            onTap: () async =>
            await FlutterWebBrowser.openWebPage(
                url: "https://devshub.co/",
                androidToolbarColor: Theme
                    .of(context)
                    .primaryColor),
            leading: Icon(Icons.assistant),
            title: Text("FAQ"),
          ),
          Divider(
            color: Colors.grey,
            height: 1.0,
          ),
          ListTile(
            onTap: () {
              _setValue('name', '');
              _setValue('email', '');
              goToWelcomeScreen(context);
            },
            leading: Icon(Icons.exit_to_app),
            title: Text("Logout"),
          ),
          SizedBox(
            height: 30,
          ),
          Container(
            alignment: Alignment.center,
            child: Column(
              children: <Widget>[
                Container(
                  width: 45.0,
                  height: 45.0,
                  child: Image.asset(
                    'assets/images/chef.png',
                    color: colorMain,
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  'Brought to you by Devshub',
                  style: TextStyle(color: colorMain),
                ),
                SizedBox(
                  height: 8.0,
                ),
                Text(
                  'Version 1.0',
                  style: TextStyle(color: Colors.grey),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
