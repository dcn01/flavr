import 'package:flavr/utils/colors.dart';
import 'package:flavr/utils/styles.dart';
import 'package:flavr/utils/navigator.dart';
import 'package:flutter/material.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorMain,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            child: Container(
              width: 110.0,
              height: 110.0,
              child: Image.asset('assets/images/chef.png'),
            ),
          ),
          SizedBox(
            height: 40,
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: Container(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Text(
                'Welcome to Flavr',
                style: TextStyle(
                    fontSize: 26, color: colorWhite, fontFamily: fontRegular),
              ),
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Container(
            width: MediaQuery.of(context).size.width * .93,
            height: 50,
            child: FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(28.0)),
              onPressed: () => goToLoginScreen(context),
              color: colorWhite,
              child: Text(
                "Login",
                style: TextStyle(
                    color: colorMain, fontSize: 16.0, fontFamily: fontRegular),
              ),
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Container(
            alignment: Alignment.center,
            child: Container(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Text(
                'OR',
                style: TextStyle(
                    fontSize: 26,
                    color: colorWhite,
                    fontWeight: FontWeight.bold,
                    fontFamily: fontBold),
              ),
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Container(
            width: MediaQuery.of(context).size.width * .93,
            height: 50,
            child: OutlineButton(
              borderSide: BorderSide(color: colorWhite),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(28.0)),
              onPressed: () => goToRegisterScreen(context),
              color: colorMain,
              child: Text(
                "Sign Up",
                style: TextStyle(
                    color: colorWhite, fontSize: 16.0, fontFamily: fontRegular),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
