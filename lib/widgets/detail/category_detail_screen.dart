import 'package:flavr/model/category.dart';
import 'package:flavr/model/recipes.dart';
import 'package:flavr/rest/provider/recipes_provider.dart';
import 'package:flavr/utils/constants.dart';
import 'package:flavr/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';

class CategoryDetailScreen extends StatefulWidget {
  Category category;

  CategoryDetailScreen(this.category);

  @override
  _CategoryDetailScreenState createState() =>
      _CategoryDetailScreenState(this.category);
}

class _CategoryDetailScreenState extends State<CategoryDetailScreen> {
  Category _category;

  _CategoryDetailScreenState(this._category);

  // Recipes
  final RProvider rProvider = RProvider();
  List<Recipes> _recipeList = List();
  int _rPageNumber = 1;
  LoadingState _rLoadingState = LoadingState.LOADING;
  bool _rIsLoading = false;

  @override
  void initState() {
    super.initState();
    _loadRecipes();
  }

  _loadRecipes() async {
    _rIsLoading = true;
    try {
      var getRecipesList =
          await rProvider.loadRecipes(_category.id, page: _rPageNumber);
      setState(() {
        _rLoadingState = LoadingState.DONE;
        _recipeList.addAll(getRecipesList);
        _rIsLoading = false;
        _rPageNumber++;
      });
    } catch (e) {
      _rIsLoading = false;
      if (_rLoadingState == LoadingState.LOADING) {
        setState(() {
          _rLoadingState = LoadingState.ERROR;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(_category.name),
        ),
        body: Center(
          child: _getRecipesContent(),
        ));
  }

  Widget _getRecipesContent() {
    switch (_rLoadingState) {
      case LoadingState.DONE:
        return ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: _recipeList.length,
            itemBuilder: (BuildContext context, int index) {
              if (!_rIsLoading && index > (_recipeList.length * 0.7)) {
                _loadRecipes();
              }
              return _recipesItems(_recipeList[index], context);
            });
      case LoadingState.ERROR:
        return GestureDetector(
          onTap: () {
            _rPageNumber = 1;
            _loadRecipes();
          },
          child: Container(
            padding: EdgeInsets.only(top: 20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.cloud_off,
                  color: Colors.redAccent,
                  size: 40.0,
                ),
                Text(
                  'No Internet Connection.',
                  style: TextStyle(fontSize: 18.0, fontFamily: fontBold),
                ),
                Text(
                  'Click to Retry',
                  style: TextStyle(color: Colors.deepOrange),
                ),
              ],
            ),
          ),
        );
      case LoadingState.LOADING:
        return CircularProgressIndicator();
      default:
        return Container();
    }
  }

  Widget _recipesItems(Recipes recipe, context) {
    return Container(
      child: Card(
        child: GestureDetector(
            onTap: () async => await FlutterWebBrowser.openWebPage(
                  url: "https://devshub.co/",
                  androidToolbarColor: Theme.of(context).primaryColor,
                ),
            child: Stack(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Hero(
                      child: FadeInImage.assetNetwork(
                        placeholder: "assets/images/recipes_placeholder.png",
                        image: recipe.photo,
                        fit: BoxFit.fill,
                        width: double.infinity,
                        fadeInDuration: Duration(milliseconds: 50),
                      ),
                      tag: "Recipe-Tag-${recipe.id}",
                    ),
                    Padding(
                      padding: EdgeInsets.all(12.0),
                      child: Text(
                        recipe.title,
                        style: TextStyle(
                            fontFamily: fontBold,
                            fontSize: 20.0,
                            color: Colors.grey,
                            fontWeight: FontWeight.w700),
                      ),
                    )
                  ],
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    padding: EdgeInsets.all(6.0),
                    child: Icon(
                      Icons.favorite_border,
                      color: Colors.redAccent,
                      size: 28,
                    ),
                  ),
                )
              ],
            )),
      ),
    );
  }
}
