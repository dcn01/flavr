class User {
  String id;
  String name;
  String email;
  String password;

  User({this.id, this.name, this.email, this.password});

  factory User.fromJson(Map<String, dynamic> json) {

    if (json == null) {
      throw FormatException("Null JSON");
    }

    return User(
      id: json['id'],
      name: json['name'],
      email: json['email'],
    );
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["name"] = name;
    map["email"] = email;
    map["password"] = password;

    return map;
  }
}
