class KitchenAccessories {
  int id;
  String title;
  String photo;

  factory KitchenAccessories(Map jsonMap) =>
      KitchenAccessories._internalFromJson(jsonMap);

  KitchenAccessories._internalFromJson(Map jsonMap)
      : id = jsonMap["id"],
        title = jsonMap["title"] ?? "",
        photo = jsonMap["photo"] ?? "";

  Map toJson() =>
      {
        'id': id,
        'title': title,
        'photo': photo
      };

  factory KitchenAccessories.fromPrefsJson(Map jsonMap) =>
      KitchenAccessories._internalFromJson(
          jsonMap
      );
}
