import 'dart:async';
import 'dart:io';

import 'package:flavr/model/favorite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DbHelper {
  static final DbHelper _dbHelper = DbHelper._internal();

  // Db Version
  int dbVersion = 1;

  // Favorite
  String tableFavorite = "favorite";
  String colFavId = "favId";
  String colFavCategoryId = "favCategoryId";
  String colFavCategoryName = "favCategoryName";
  String colFavTitle = "favTitle";
  String colFavPhoto = "favPhoto";

  DbHelper._internal();

  factory DbHelper() {
    return _dbHelper;
  }

  static Database _db;

  Future<Database> get db async {
    if (_db == null) {
      _db = await initializeDb();
    }
    return _db;
  }

  Future<Database> initializeDb() async {
    Directory dir = await getApplicationDocumentsDirectory();
    String path = dir.path + "flavr.db";

    var dbFlavr =
        await openDatabase(path, version: dbVersion, onCreate: _createDb);
    return dbFlavr;
  }

  void _createDb(Database db, int newVersion) async {
    await db.execute(
        "CREATE TABLE $tableFavorite($colFavId INTEGER PRIMARY KEY, $colFavCategoryId INTEGER, " +
            "$colFavCategoryName TEXT, $colFavTitle TEXT, $colFavPhoto TEXT)");
  }

  Future<int> insertRecipe(Favorite fav) async {
    int result = 0;
    Database db = await this.db;
    var checkId = await db
        .rawQuery('SELECT * FROM $tableFavorite WHERE $colFavId = ${fav.id}');
    if (checkId == null || checkId.isEmpty) {
       result = await db.insert(tableFavorite, fav.toMap());
    }
    return result;
  }

  Future<List<Map>> getRecipes() async {
    Database db = await this.db;
    var result = await db
        .rawQuery('SELECT * FROM $tableFavorite order by $colFavId ASC');
    return result;
  }

  Future<int> deleteRecipe(int id) async {
    Database db = await this.db;
    var result =
        await db.rawDelete('DELETE FROM $tableFavorite WHERE $colFavId = $id');
    return result;
  }
}
